# alpine-gpio
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-gpio)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-gpio)



----------------------------------------
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-gpio/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : aarch64
* Appplication : [WiringPi](http://wiringpi.com/)
    - WiringPi is a GPIO access library written in C for the BCM2835 used in the Raspberry Pi.



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           --privileged \
           --device=/dev/mem \
		   -e SBC=(rpi|bpi|opih3) \
           forumi0721/alpine-gpio:[ARCH_TAG]
```



----------------------------------------
#### Usage

```
# gpio
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| --privileged       | Need for gpio access                             |
| --device=/dev/mem  | Need for gpio access                             |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| SBC                | SBC Type (rpi, bpi. opih3)                       |

